﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLDSV_FINAL
{
    public partial class formLogin : Form
    {
        public formLogin()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Program.servername = comboBox1.SelectedValue.ToString();
                label4.Text = Program.servername;
            }
            catch (Exception) { };

        }

        private void formLogin_Load(object sender, EventArgs e)
        {
            string chuoiketnoi = "Data Source=DESKTOP-PH56ABS;Initial Catalog=QLDSV;Integrated Security=True";

            Program.conn.ConnectionString = chuoiketnoi;
            Program.conn.Open();
            DataTable dt = new DataTable();
            dt = Program.ExecSqlDataTable("SELECT * FROM V_DS_PHANMANH");
            Program.bds_dspm.DataSource = dt;
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "TENCN";
            comboBox1.ValueMember = "TENSERVER";
            comboBox1.SelectedIndex = -1;
            label4.Text = "";


        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            if (txtUser.Text.Trim() == "" || txtPass.Text.Trim() == "")
            {
                MessageBox.Show("Login name và mật mã không được trống", "", MessageBoxButtons.OK);
                return;
            }
            if (comboBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Hãy Chọn Khoa Muốn Đăng Nhập Trước !", "", MessageBoxButtons.OK);
                return;
            }
            Program.mlogin = txtUser.Text; Program.password = txtPass.Text;
            if (Program.KetNoi() == 0) return;

            Program.mChinhanh = comboBox1.SelectedIndex;
            
            Program.mloginDN = Program.mlogin;
            Program.passwordDN = Program.password;
            string strLenh = "EXEC SP_DANGNHAP '" + Program.mlogin + "'";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null) return;
            Program.myReader.Read();


            Program.username = Program.myReader.GetString(0);     // Lay user name
            if (Convert.IsDBNull(Program.username))
            {
                MessageBox.Show("Login bạn nhập không có quyền truy cập dữ liệu\n Bạn xem lại username, password", "", MessageBoxButtons.OK);
                return;
            }
            Program.mHoten = Program.myReader.GetString(1);
            Program.mGroup = Program.myReader.GetString(2);
            Program.myReader.Close();
            Program.conn.Close();
         //   MessageBox.Show("Giang Vien - Nhom : " + Program.mHoten + " - " + Program.mGroup, "", MessageBoxButtons.OK);
            this.Hide();
            FormMain fm = new FormMain();
            fm.Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
      
        {
            DialogResult dialog = MessageBox.Show("Bạn có chắc chắn muốn thoát ?", "Thoát", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                this.Close();
            }
            
            
        }
    }
}
